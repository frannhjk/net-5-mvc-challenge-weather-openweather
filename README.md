# README #

Proyecto desarrollado en .NET 5.0, formato MVC, consume un microservicio que pueden encontrar en este Repository, tambien desarrollado
en .NET 5.0 con C# 9.
El microservicio consume una API Externa llamada OpenWeather.

# Features #

Aplicación MVC 

* Framework .NET 5.0.
* Version del lenguaje C# 9.
* Utilización de Records (C# 9) y lectura de json (C# 9) [Microservicio]
* Abstracción de la capa de datos y lógica de negocio (Proyecto Infrastructure y Proyecto Core).
* Automapper para mapeo de Entidades y DTOs.
* Utilización de inyección de dependencias.
* ViewComponents
* Consumo de API Externa en un microservicio desarrolado por mi.


### Requisitos ##
* 1. Clone Microservicio:  $ git clone https://frannhjk@bitbucket.org/frannhjk/.net-5-microservices-openweather.git
* 2. Clone Aplicación MVC:  $ git clone https://frannhjk@bitbucket.org/frannhjk/.net-5-mvc-challenge-weather.git


### Como inciarlo? ###
* Primero iniciar el Microservicio .NET 5 - Microservices: OpenWeather: # dotnet run 
* Iniciar la solución: 'Challenge Weather'
* Se puede probar el microservicio utilizando Postman al siguiente Endpoint: http://localhost:5000/WeatherForecast/Buenos Aires