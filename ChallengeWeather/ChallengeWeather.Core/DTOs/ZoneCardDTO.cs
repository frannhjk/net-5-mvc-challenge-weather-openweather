﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChallengeWeather.Core.DTOs
{
    public class ZoneCardDTO
    {
        public int Id { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    }
}
