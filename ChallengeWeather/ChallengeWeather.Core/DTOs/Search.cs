﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChallengeWeather.Core.DTOs
{
    public class Search
    {
        public string SearchedCity { get; set; }

        public Search(string searchedCity)
        {
            SearchedCity = searchedCity;
        }
    }
}
