﻿using AutoMapper;
using ChallengeWeather.Core.DTOs;
using ChallengeWeather.Core.Interfaces;
using ChallengeWeather.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeWeather.Core.Services
{
    public class CardService : ICardService
    {
        private readonly ICardRepository _zoneCardRepository;
        private readonly IMapper _mapper;


        public CardService(ICardRepository zoneCardRepository, IMapper mapper)
        {
            _zoneCardRepository = zoneCardRepository;
            _mapper = mapper;
        }

        public IEnumerable<ZoneCardDTO> GetZoneCardsMockFromRepo()
        {
            var listDTO = new List<ZoneCardDTO>();

            var entity = _zoneCardRepository.GetZoneCardsMock().ToList();

            entity.ForEach(a =>
            {
                var dto = new ZoneCardDTO
                {
                    Country = a.Country,
                    City = a.City
                };
                listDTO.Add(dto);
            });

            return listDTO;
        }

        public async Task<CardDTO> GetCardsDataFromRepo(string city)
        {
            //GetFromJsonAsync<Card>($"localhost:5000/WeatherForecast/{char.ToUpper(city[0]) + city.Substring(1)}");

            var forecast = await _zoneCardRepository.GetCardsDataFromAPIAsync(city);

            var resultDto = _mapper.Map<CardDTO>(forecast);

            resultDto.Description = char.ToUpper(resultDto.Description[0]) + resultDto.Description.Substring(1);

            //resultDto = forecast.Date.DayOfWeek.ToString();

            var day = resultDto.Date.ToString("dddd");

            resultDto.DateShort = char.ToUpper(day[0]) + day.Substring(1);

            return resultDto;
        }
    }
}
