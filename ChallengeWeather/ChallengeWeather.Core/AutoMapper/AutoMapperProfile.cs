﻿using AutoMapper;
using ChallengeWeather.Core.DTOs;
using ChallengeWeather.Infrastructure.Models;

namespace ChallengeWeather.Core.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Card, CardDTO>();
        }
    }
}
