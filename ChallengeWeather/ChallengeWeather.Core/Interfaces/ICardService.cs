﻿using ChallengeWeather.Core.DTOs;
using ChallengeWeather.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeWeather.Core.Interfaces
{
    public interface ICardService
    {
        IEnumerable<ZoneCardDTO> GetZoneCardsMockFromRepo();
        Task<CardDTO> GetCardsDataFromRepo(string city);
    }
}
