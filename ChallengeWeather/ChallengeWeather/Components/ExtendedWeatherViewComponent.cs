﻿using ChallengeWeather.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeWeather.Components
{
    public class ExtendedWeatherViewComponent : ViewComponent
    {
        private readonly ICardService _zoneCardService;

        public ExtendedWeatherViewComponent(ICardService zoneCardService)
        {
            _zoneCardService = zoneCardService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            string cityToReturn = "Buenos Aires";

            if (TempData.ContainsKey("extendedData"))
                cityToReturn = TempData["extendedData"].ToString();

            var forecast = await _zoneCardService.GetCardsDataFromRepo(cityToReturn);

            return View(forecast);
        }
    }
}
