﻿using ChallengeWeather.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ChallengeWeather.Components
{
    public class ZoneCardViewComponent : ViewComponent
    {
        private readonly ICardService _zoneCardService;

        public ZoneCardViewComponent(ICardService zoneCardService)
        {
            _zoneCardService = zoneCardService;
        }

        public async Task<IViewComponentResult> InvokeAsync(string searchedCity)
        {
            var cities = _zoneCardService.GetZoneCardsMockFromRepo();

            //if (searchedCity is not null)
            //{
            //    return View("ReportCard");
            //}

            return View(cities);
        }

        //[HttpPost]
        //public async Task<IViewComponentResult> test(string searchedCity)
        //{
        //    ViewData["City"] = searchedCity;

        //    return View();
        //}

        //POST Method con la ciudad (Recibirá una ciudad que debo guardar y enviar al ReportCardViewComponent).

    }
}
