﻿using ChallengeWeather.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ChallengeWeather.Components
{
    public class ReportCardViewComponent : ViewComponent
    {
        private readonly ICardService _zoneCardService;

        public ReportCardViewComponent(ICardService zoneCardService)
        {
            _zoneCardService = zoneCardService;
        }

        // Utilizará un TempData
        public async Task<IViewComponentResult> InvokeAsync(string searchedCity)
        {
            CultureInfo originalCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-ES");


            string cityToReturn = "Buenos Aires";

            //TempData["searchedCity"]

            if (TempData.ContainsKey("searchedCity"))
                cityToReturn = TempData["searchedCity"].ToString(); 

            var forecast = await _zoneCardService.GetCardsDataFromRepo(cityToReturn);


            TempData["extendedData"] = cityToReturn;

            return View(forecast);
        }
    }
}
