﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeWeather.Models
{
    public class ZoneCard
    {
        public string Country { get; set; }
        public string City { get; set; }
    }
}
