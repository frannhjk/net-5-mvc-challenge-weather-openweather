﻿using ChallengeWeather.Core.Interfaces;
using ChallengeWeather.Infrastructure.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeWeather.Infrastructure.Repository
{
    public class CardRepository : ICardRepository
    {
        private readonly HttpClient _httpClient;

        public CardRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public IEnumerable<ZoneCard> GetZoneCardsMock()
        {

            var card1 = new ZoneCard()
            {
                Country = "Argentina",
                City = "Buenos Aires"
            };

            var card2 = new ZoneCard()
            {
                Country = "GB",
                City = "London"
            };

            var card3 = new ZoneCard()
            {
                Country = "España",
                City = "Barcelona"
            };

            var card4 = new ZoneCard()
            {
                Country = "Argentina",
                City = "Capitan Sarmiento"
            };

            var card5 = new ZoneCard()
            {
                Country = "Finlandia",
                City = "Helsinki"
            };

            var list = new List<ZoneCard>();

            list.Add(card1);
            list.Add(card2);
            list.Add(card3);
            list.Add(card4);
            list.Add(card5);

            return list;
        }

        
        public async Task<Card> GetCardsDataFromAPIAsync(string city)
        {
            try
            {
                var request = await _httpClient.GetAsync($"http://localhost:5000/WeatherForecast/{char.ToUpper(city[0]) + city.Substring(1)}");

                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().GetAwaiter().GetResult();

                    Card resultEntity = JsonConvert.DeserializeObject<Card>(result);

                    return resultEntity;
                }
            }
            catch(Exception ex)
            {
                return new Card();

            }
            return new Card();
        }
    }
}
