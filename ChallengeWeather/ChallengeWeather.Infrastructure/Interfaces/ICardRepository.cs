﻿using ChallengeWeather.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChallengeWeather.Core.Interfaces
{
    public interface ICardRepository
    {
        IEnumerable<ZoneCard> GetZoneCardsMock();
        Task<Card> GetCardsDataFromAPIAsync(string city);
    }
}
