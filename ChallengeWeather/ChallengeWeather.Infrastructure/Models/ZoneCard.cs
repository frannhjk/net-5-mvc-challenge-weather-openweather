﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChallengeWeather.Infrastructure.Models
{
    public class ZoneCard
    {
        public int Id { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    }
}
