﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChallengeWeather.Infrastructure.Models
{
    public class Card
    {
        public DateTime Date { get; set; }
        public string Main { get; set; }
        public string Description { get; set; }
        public int TemperatureC { get; set; }
        public int TemperatureFeelsLike { get; set; }
        public int TempMin { get; set; }
        public int TempMax { get; set; }
        public int Pressure { get; set; }
        public int Humidity { get; set; }
        public int WindSpeed { get; set; }
        public string Name { get; set; }
    }
}
